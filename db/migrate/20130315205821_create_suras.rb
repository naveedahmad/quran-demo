class CreateSuras < ActiveRecord::Migration
  def change
    create_table :suras do |t|
      t.string :name
      t.references :para
    end
    add_index :suras, :para_id
  end
end
