class CreateWordFrequencies < ActiveRecord::Migration
  def change
    create_table :word_frequencies do |t|
      t.references :word
      t.integer :frequency
      t.integer :wordable_id
      t.string :wordable_type

      t.timestamps
    end
    add_index :word_frequencies, :word_id
  end
end
