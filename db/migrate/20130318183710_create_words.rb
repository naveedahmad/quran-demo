class CreateWords < ActiveRecord::Migration
  def change
    create_table :words do |t|
      t.string :content

      t.timestamps
    end
    add_index :words, :content, length: 100
  end
end
