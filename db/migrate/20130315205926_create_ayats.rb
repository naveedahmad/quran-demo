class CreateAyats < ActiveRecord::Migration
  def change
    create_table :ayats do |t|
      t.text :content
      t.text :arabic_content
      t.references :sura
      t.integer :ayat_number

    end
    add_index :ayats, :sura_id
  end
end
