# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130318184212) do

  create_table "ayats", :force => true do |t|
    t.text    "content"
    t.text    "arabic_content"
    t.integer "sura_id"
    t.integer "ayat_number"
  end

  add_index "ayats", ["sura_id"], :name => "index_ayats_on_sura_id"

  create_table "paras", :force => true do |t|
    t.string "name"
  end

  create_table "suras", :force => true do |t|
    t.string  "name"
    t.integer "para_id"
  end

  add_index "suras", ["para_id"], :name => "index_suras_on_para_id"

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

  create_table "word_frequencies", :force => true do |t|
    t.integer  "word_id"
    t.integer  "frequency"
    t.integer  "wordable_id"
    t.string   "wordable_type"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "word_frequencies", ["word_id"], :name => "index_word_frequencies_on_word_id"

  create_table "words", :force => true do |t|
    t.text     "content"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "words", ["content"], :name => "index_words_on_content", :length => {"content"=>100}

end
