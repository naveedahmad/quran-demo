require 'test_helper'

class SurasControllerTest < ActionController::TestCase
  setup do
    @sura = suras(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:suras)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create sura" do
    assert_difference('Sura.count') do
      post :create, sura: { name: @sura.name }
    end

    assert_redirected_to sura_path(assigns(:sura))
  end

  test "should show sura" do
    get :show, id: @sura
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @sura
    assert_response :success
  end

  test "should update sura" do
    put :update, id: @sura, sura: { name: @sura.name }
    assert_redirected_to sura_path(assigns(:sura))
  end

  test "should destroy sura" do
    assert_difference('Sura.count', -1) do
      delete :destroy, id: @sura
    end

    assert_redirected_to suras_path
  end
end
