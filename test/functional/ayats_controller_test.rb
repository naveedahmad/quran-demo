require 'test_helper'

class AyatsControllerTest < ActionController::TestCase
  setup do
    @ayat = ayats(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:ayats)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create ayat" do
    assert_difference('Ayat.count') do
      post :create, ayat: { ayat_num: @ayat.ayat_num, content: @ayat.content }
    end

    assert_redirected_to ayat_path(assigns(:ayat))
  end

  test "should show ayat" do
    get :show, id: @ayat
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @ayat
    assert_response :success
  end

  test "should update ayat" do
    put :update, id: @ayat, ayat: { ayat_num: @ayat.ayat_num, content: @ayat.content }
    assert_redirected_to ayat_path(assigns(:ayat))
  end

  test "should destroy ayat" do
    assert_difference('Ayat.count', -1) do
      delete :destroy, id: @ayat
    end

    assert_redirected_to ayats_path
  end
end
