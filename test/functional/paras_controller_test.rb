require 'test_helper'

class ParasControllerTest < ActionController::TestCase
  setup do
    @para = paras(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:paras)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create para" do
    assert_difference('Para.count') do
      post :create, para: { name: @para.name }
    end

    assert_redirected_to para_path(assigns(:para))
  end

  test "should show para" do
    get :show, id: @para
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @para
    assert_response :success
  end

  test "should update para" do
    put :update, id: @para, para: { name: @para.name }
    assert_redirected_to para_path(assigns(:para))
  end

  test "should destroy para" do
    assert_difference('Para.count', -1) do
      delete :destroy, id: @para
    end

    assert_redirected_to paras_path
  end
end
