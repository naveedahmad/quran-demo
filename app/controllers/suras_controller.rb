class SurasController < ApplicationController
  # GET /suras
  # GET /suras.json
  def index
    if params[:para_id]
      @para = Para.find params[:para_id]
      @suras = @para.suras
    end
    @suras = (@sura || Sura).paginate(per_page: 50, page: params[:page])


    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @suras }
    end
  end

  # GET /suras/1
  # GET /suras/1.json
  def show
    @sura = Sura.find(params[:id])
    @ayats = @sura.ayats.paginate(per_page: 30, page: params[:page])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @sura }
    end
  end

  # GET /suras/new
  # GET /suras/new.json
  def new
    @sura = Sura.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @sura }
    end
  end

  # GET /suras/1/edit
  def edit
    @sura = Sura.find(params[:id])
  end

  # POST /suras
  # POST /suras.json
  def create
    @sura = Sura.new(params[:sura])

    respond_to do |format|
      if @sura.save
        format.html { redirect_to @sura, notice: 'Sura was successfully created.' }
        format.json { render json: @sura, status: :created, location: @sura }
      else
        format.html { render action: "new" }
        format.json { render json: @sura.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /suras/1
  # PUT /suras/1.json
  def update
    @sura = Sura.find(params[:id])

    respond_to do |format|
      if @sura.update_attributes(params[:sura])
        format.html { redirect_to @sura, notice: 'Sura was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @sura.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /suras/1
  # DELETE /suras/1.json
  def destroy
    @sura = Sura.find(params[:id])
    @sura.destroy

    respond_to do |format|
      format.html { redirect_to suras_url }
      format.json { head :no_content }
    end
  end
end
