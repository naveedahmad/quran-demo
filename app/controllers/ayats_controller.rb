class AyatsController < ApplicationController
  # GET /ayats
  # GET /ayats.json
  def index
    if params[:sura_id]
      @sura = Sura.find params[:sura_id]
      @ayats = @sura.ayats
    end
    @ayats = (@ayats || Ayat).paginate(per_page: 50, page: params[:page])

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @ayats }
    end
  end

  # GET /ayats/1
  # GET /ayats/1.json
  def show
    @ayat = Ayat.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @ayat }
    end
  end

  # GET /ayats/new
  # GET /ayats/new.json
  def new
    @ayat = Ayat.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @ayat }
    end
  end

  # GET /ayats/1/edit
  def edit
    @ayat = Ayat.find(params[:id])
  end

  # POST /ayats
  # POST /ayats.json
  def create
    @ayat = Ayat.new(params[:ayat])

    respond_to do |format|
      if @ayat.save
        format.html { redirect_to @ayat, notice: 'Ayat was successfully created.' }
        format.json { render json: @ayat, status: :created, location: @ayat }
      else
        format.html { render action: "new" }
        format.json { render json: @ayat.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /ayats/1
  # PUT /ayats/1.json
  def update
    @ayat = Ayat.find(params[:id])

    respond_to do |format|
      if @ayat.update_attributes(params[:ayat])
        format.html { redirect_to @ayat, notice: 'Ayat was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @ayat.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /ayats/1
  # DELETE /ayats/1.json
  def destroy
    @ayat = Ayat.find(params[:id])
    @ayat.destroy

    respond_to do |format|
      format.html { redirect_to ayats_url }
      format.json { head :no_content }
    end
  end
end
