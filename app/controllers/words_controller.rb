class WordsController < ApplicationController
  def index
    @words = Word.paginate per_page: 50, page: params['page']
  end

  def show
    @word = Word.find params[:id]
    @suras = @word.suras
    @ayats = @word.ayats.paginate per_page: 20, page: params[:page]
  end
end
