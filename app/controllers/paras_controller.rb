class ParasController < ApplicationController
  # GET /paras
  # GET /paras.json
  def index
    @paras = Para.paginate(per_page: 10, page: params[:page])

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @paras }
    end
  end

  # GET /paras/1
  # GET /paras/1.json
  def show
    @para = Para.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @para }
    end
  end

  # GET /paras/new
  # GET /paras/new.json
  def new
    @para = Para.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @para }
    end
  end

  # GET /paras/1/edit
  def edit
    @para = Para.find(params[:id])
  end

  # POST /paras
  # POST /paras.json
  def create
    @para = Para.new(params[:para])

    respond_to do |format|
      if @para.save
        format.html { redirect_to @para, notice: 'Para was successfully created.' }
        format.json { render json: @para, status: :created, location: @para }
      else
        format.html { render action: "new" }
        format.json { render json: @para.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /paras/1
  # PUT /paras/1.json
  def update
    @para = Para.find(params[:id])

    respond_to do |format|
      if @para.update_attributes(params[:para])
        format.html { redirect_to @para, notice: 'Para was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @para.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /paras/1
  # DELETE /paras/1.json
  def destroy
    @para = Para.find(params[:id])
    @para.destroy

    respond_to do |format|
      format.html { redirect_to paras_url }
      format.json { head :no_content }
    end
  end
end
