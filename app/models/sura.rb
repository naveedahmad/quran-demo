class Sura < ActiveRecord::Base
  belongs_to :para
  has_many :ayats
  has_many :word_frequencies, as: :wordable

  attr_accessible :name
end
