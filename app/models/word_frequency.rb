class WordFrequency < ActiveRecord::Base
  belongs_to :word
  belongs_to :wordable, polymorphic: true

  attr_accessible :frequency, :wordable_id, :wordable_type

  scope :for_ayat, where(wordable_type: 'Ayat')
  scope :for_sura, where(wordable_type: 'Sura')
end
