class Word < ActiveRecord::Base
  has_many :word_frequencies, include: :wordable

  attr_accessible :content

  validates :content, presence: true

  def ayats_count
    ayats.size
  end

  def suras_count
    suras.size
  end

  def ayats
    Ayat.where id: word_frequencies.for_ayat.pluck(:wordable_id)
  end

  def suras
    Sura.where id: word_frequencies.for_sura.pluck(:wordable_id)
  end
end
