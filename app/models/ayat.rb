class Ayat < ActiveRecord::Base
  belongs_to :sura
  has_many :word_frequencies, as: :wordable

  attr_accessible :ayat_number, :content, :sura_id, :arabic_content

  def para_name
    sura.para.name
  end

  def sura_name
    sura.name
  end

end
