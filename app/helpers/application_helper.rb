module ApplicationHelper
  def current_sura_name(prefix = nil)
    if @sura
      "#{prefix} #{@sura.try :name}"
    end
  end

  def current_para_name(perfix = nil)
    if @para
      "#{prefix} #{@para.try :name}"
    end
  end
end
