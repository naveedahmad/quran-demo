#!/bin/env ruby
# encoding: utf-8

namespace :parser do
  desc "Seed DB with Quran text"
  task :parse => :environment do
    Para.delete_all
    Sura.delete_all
    Ayat.delete_all
    current_surat = nil
    reg= /\([۱۲۳۴۵۶۷۸۹۰]*\)/
    ayat_number = nil
    ['1.txt', '2.txt'].each do |file|
      open("#{Rails.root}/db/AlQuran/#{file}").each_line do |line|
        if line.include? "سورة"
          if current_surat
            puts "last sura was #{current_surat.id}- #{current_surat.name} total ayats #{current_surat.ayats.count}"
          end
          ayat_number = 1
          current_surat = Sura.create name: line.chomp
          next
        end

        next if line.blank?

        if current_surat
          p "Sura #{current_surat.id}"+ current_surat.name


          line.chomp!
          line.split(reg).each do |ayat_content|
            if ayat_content.present?
              p "ayat" + ayat_number.to_s
              current_surat.ayats.create content: ayat_content, ayat_number: ayat_number
              ayat_number += 1
            end
          end
        end
      end
    end

    puts "parsing arabic translation"
    text = open("#{Rails.root}/db/AlQuran/quran-simple.xml").read
    bismillah = false

    Hash.from_xml(text)["quran"]["sura"].each do |sura_xml|
      if sura = Sura.find_by_id(sura_xml['index'])
        p "parsing sura #{sura.name}  arabic name #{sura_xml['name']}"
        sura_xml["aya"].each do |ayat_xml|
          if ayat_xml["index"] == "1" #this is Bismillah we can ignore it
            bismillah_text = ayat_xml['bismillah']
            text = ayat_xml['text']

            if bismillah = bismillah_text.present?
              sura.ayats.find_by_ayat_number(1).update_attribute :arabic_content, bismillah_text
            end

          else
            text = ayat_xml['text']
          end
          ayat_number =   ayat_xml["index"].to_i
          ayat_number += 1 if bismillah

          if ayat = sura.ayats.find_by_ayat_number(ayat_number)
            p "ayat #{ayat.id}"
            ayat.update_attribute :arabic_content, text
          end
        end
      end
    end
  end

  desc "seed words frequency"
  task :seed_words_frequency => :environment do
    Word.delete_all

    Ayat.find_each do |ayat|
      words = ayat.content.split(' ')
      words.each do |word|
        uniq_word = Word.find_or_create_by_content word
        word_frequency = ayat.word_frequencies.find_or_create_by_word_id uniq_word.id
        word_frequency.update_attribute :frequency, word_frequency.frequency.to_i + 1
      end

      puts "frequency for ayat #{ayat.id} is #{ayat.word_frequencies.size}"
    end

    Sura.find_each do |sura|
      words = sura.name.split(' ')
      words.each do |word|
        next if word.blank?

        uniq_word = Word.find_or_create_by_content word.gsub(/[)|(]/, '')
        word_frequency = sura.word_frequencies.find_or_create_by_word_id uniq_word.id
        word_frequency.update_attribute :frequency, word_frequency.frequency + 1
      end

      puts "frequency for sura #{sura.id} is #{sura.word_frequencies.size}"
    end
  end
end





